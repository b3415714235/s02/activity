year = int(input("Please input a year: "))


if year <= 0:
	print(f"year should not be a negative number or zero")
	
elif year % 4 == 0:
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year")


row = int(input("Enter number of rows: "))
column = int(input("Enter number of columns: "))


for i in range(column):
    asterisk = ""
    for i in range(row):
        asterisk += "*"
    print(asterisk)
